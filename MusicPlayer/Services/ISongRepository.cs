﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MusicPlayer.Models;

namespace MusicPlayer.Services
{
    public interface ISongRepository : IRepository<SongDB>
    {
        //[GET]
        Task<IEnumerable<SongDB>> GetAll();

        //[GET]/id
        Task<SongDB> Get(long id);

        //[POST]
        Task Create(SongDB songCreate);

        //[PUT]/id
        Task<bool> Update(SongDB song);

        //SongWID PartiallyUpdate(int id, JsonPatchDocument<SongWID> patchDoc);

        //[DELETE]/id
        Task<bool> Delete(long id);

        Task<long> GetNextId();
    }
}
