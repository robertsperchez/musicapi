﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;

namespace MusicPlayer.Services
{
    public class FileReader : IFileReader
    {
        private IHostingEnvironment _hostingEnvironment;

        public FileReader(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public Stream GetFile(string filePath)
        {
            FileStream fileStream = new FileStream(filePath, FileMode.Open);

            return fileStream;
        }
    }
}
