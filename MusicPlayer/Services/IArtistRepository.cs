﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MusicPlayer.Models;

namespace MusicPlayer.Services
{
    public interface IArtistRepository : IRepository<ArtistDB>
    {
        //[GET]
        Task<IEnumerable<ArtistDB>> GetAll();

        //[GET]/id
        Task<ArtistDB> Get(long id);

        //[POST]
        Task Create(ArtistDB artistCreate);

        //[PUT]/id
        Task<bool> Update(ArtistDB artist);

        //[PATCH]/id
        //Task<bool> PartiallyUpdate(long id, JsonPatchDocument<ArtistDB> patchDoc);

        //[DELETE]/id
        Task<bool> Delete(long id);

        Task<long> GetNextId();
    }
}
