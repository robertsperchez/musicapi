﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicPlayer.Exceptions
{
    public class InexistingIDException : Exception
    {
        public InexistingIDException()
        {
        }

        public InexistingIDException(string message)
            : base(message)
        {
        }

        public InexistingIDException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
