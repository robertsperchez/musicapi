﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MusicPlayer.Models;
using DeleteResult = MongoDB.Driver.DeleteResult;

namespace MusicPlayer.Services
{
    public class SongRepository : ISongRepository
    {
        private readonly IMusicAPIContext _context;
        public SongRepository(IMusicAPIContext context)
        {
            _context = context;
        }

        public async Task Create(SongDB songCreate)
        {
            await _context.SongsCollection.InsertOneAsync(songCreate);
        }

        public async Task<bool> Delete(long id)
        {
            MongoDB.Driver.FilterDefinition<SongDB> filter = MongoDB.Driver.Builders<SongDB>.Filter.Eq(s => s.Id, id);
            DeleteResult deleteResult = await _context.SongsCollection.DeleteOneAsync(filter);
            return deleteResult.IsAcknowledged && deleteResult.DeletedCount > 0;
        }

        public async Task<IEnumerable<SongDB>> GetAll()
        {
            return await _context.SongsCollection.Find(_ => true).ToListAsync();
        }

        public async Task<SongDB> Get(long id)
        {
            MongoDB.Driver.FilterDefinition<SongDB> filter = MongoDB.Driver.Builders<SongDB>.Filter.Eq(s => s.Id, id);
            return await _context.SongsCollection.Find(filter).FirstOrDefaultAsync();
        }

        public async Task<long> GetNextId()
        {
            return await _context.SongsCollection.CountDocumentsAsync(new BsonDocument()) + 1;
        }

        public async Task<bool> Update(SongDB song)
        {
            ReplaceOneResult updateResult = await _context.SongsCollection.ReplaceOneAsync(filter: s => s.Id == song.Id, replacement: song);
            return updateResult.IsAcknowledged && updateResult.ModifiedCount > 0;
        }

        /*public SongWID PartiallyUpdate(int id, JsonPatchDocument<SongWID> patchDoc)
        {
            SongDto songFromStore = SongsDataStore.Current.Songs.FirstOrDefault(s => s.Id == id);

            if (songFromStore == null)
            {
                throw new System.Exception("There is no song with this id!");
            }

            SongWID songToPatch = _mapper.Map<SongWID>(songFromStore);

            patchDoc.ApplyTo(songToPatch);

            _mapper.Map(songToPatch, songFromStore);

            return songToPatch;
        }*/
    }
}
