﻿using System.Collections.Generic;

namespace MusicPlayer.Models
{
    using MongoDB.Bson;
    using MongoDB.Bson.Serialization.Attributes;

    public class ArtistDB
    {
        [BsonId]
        public ObjectId InternalId { get; set; }
        public long Id { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public List<string> Genres { get; set; }
        public List<string> Members { get; set; }
    }
}