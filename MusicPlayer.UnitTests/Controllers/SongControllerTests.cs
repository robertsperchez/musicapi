﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using MusicPlayer.Controllers;
using MusicPlayer.Models;
using MusicPlayer.Services;
using NUnit.Framework;

namespace MusicPlayer.UnitTests.Controllers
{
    [TestFixture]
    class SongControllerTests
    {
        private Mock<ISongRepository> _mockSongRepository;
        private Mock<IArtistRepository> _mockArtistRepository;
        private SongsController _songController;
        private ArtistsController _artistController;

        [SetUp]
        public void Setup()
        {
            _mockSongRepository = new Mock<ISongRepository>();
            _mockArtistRepository = new Mock<IArtistRepository>();
            _artistController = new ArtistsController(_mockArtistRepository.Object);
            _songController = new SongsController(_mockSongRepository.Object, _mockArtistRepository.Object);
        }

        [TearDown]
        public void Cleanup()
        {
            _songController = null;
            _artistController = null;
        }

        [Test]
        public async Task GetAllTest()
        {
            // Arrange
            IEnumerable<SongDB> songs = new List<SongDB>();

            _mockSongRepository.Setup(m => m.GetAll()).ReturnsAsync(songs);
            _songController = new SongsController(_mockSongRepository.Object, _mockArtistRepository.Object);

            // Act 
            ActionResult<IEnumerable<SongDB>> a = await _songController.GetAllSongs();

            // Assert
            Assert.IsNotNull(a);
            Assert.IsInstanceOf<ObjectResult>(a.Result);

            ObjectResult result = a.Result as ObjectResult;
            Assert.IsNotNull(result);

            Assert.IsInstanceOf<List<SongDB>>(result.Value);

            List<SongDB> aa = result.Value as List<SongDB>;

            Assert.AreEqual(songs.ToList().Count, aa.Count);
            CollectionAssert.AreEqual(songs, aa);
        }

        [Test]
        public async Task GetSongTest()
        {
            SongDB song = new SongDB();
            _mockSongRepository.Setup(mock => mock.Get(It.IsAny<long>())).ReturnsAsync(song);
            _songController = new SongsController(_mockSongRepository.Object, _mockArtistRepository.Object);

            ActionResult<SongDB> a = await _songController.GetSong(1);

            Assert.IsNotNull(a);
            Assert.IsInstanceOf<ActionResult>(a.Result);

            ObjectResult result = a.Result as ObjectResult;
            Assert.IsNotNull(result);

            Assert.AreEqual((int)HttpStatusCode.OK, result.StatusCode);
        }

        [Test]
        public async Task GetSongNullTest()
        {
            SongDB song = new SongDB();
            _songController = new SongsController(_mockSongRepository.Object, _mockArtistRepository.Object);

            ActionResult<SongDB> a = await _songController.GetSong(1);

            Assert.IsNotNull(a);

            NotFoundResult result = a.Result as NotFoundResult;

            Assert.AreEqual((int)HttpStatusCode.NotFound, result.StatusCode);
        }

        [Test]
        public async Task DeleteSongTest()
        {
            SongDB song = new SongDB();
            _mockSongRepository.Setup(mock => mock.Get(It.IsAny<long>())).ReturnsAsync(song);
            _mockSongRepository.Setup(mock => mock.Delete(It.IsAny<long>())).ReturnsAsync(true);
            _songController = new SongsController(_mockSongRepository.Object, _mockArtistRepository.Object);

            ActionResult a = await _songController.DeleteSong(song.Id);

            ObjectResult result = a as ObjectResult;

            Assert.AreEqual((int)HttpStatusCode.OK, result.StatusCode);
            Assert.IsTrue((bool)result.Value);
        }

        [Test]
        public async Task DeleteSongNotFoundTest()
        {
            _songController = new SongsController(_mockSongRepository.Object, _mockArtistRepository.Object);

            ActionResult a = await _songController.DeleteSong(1);
            NotFoundResult result = a as NotFoundResult;

            Assert.AreEqual((int)HttpStatusCode.NotFound, result.StatusCode);
        }

        [Test]
        public async Task CreateSongBadRequestTest()
        {
            SongDB song = new SongDB();
            _mockSongRepository.Setup(mock => mock.Create(song));
            _songController = new SongsController(_mockSongRepository.Object, _mockArtistRepository.Object);

            ActionResult<SongDB> a = await _songController.CreateSong(song);

            Assert.IsNotNull(a);

            BadRequestResult result = a.Result as BadRequestResult;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, result.StatusCode);
        }
    }
}
