﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicPlayer.Exceptions
{
    public class IncompleteInformationException : Exception
    {
        public IncompleteInformationException()
        {
        }

        public IncompleteInformationException(string message)
            : base(message)
        {
        }

        public IncompleteInformationException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}

