﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MusicPlayer.Exceptions;
using MusicPlayer.Models;
using MusicPlayer.Services;

namespace MusicPlayer.Controllers
{
    [Produces("application/json")]
    [Route("songs")]
    public class SongsController : Controller
    {
        private ISongRepository _repoSong;
        private IFileReader _fileReader;
        private IArtistRepository _artistRepo;
        public SongsController(ISongRepository repositorySong, IArtistRepository repositoryArtist, IFileReader fileReader)
        {
            _repoSong = repositorySong;
            _artistRepo = repositoryArtist;
            _fileReader = fileReader;
        }

        /// <summary>
        /// Returns all the songs.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SongDB>>> GetAllSongs()
        {
            return new ObjectResult(await _repoSong.GetAll());
        }

        /// <summary>
        /// Returns a song with a specific id.
        /// If the song is not found, returns a 404.
        /// </summary>
        /// <param name="id">The id of the song we want to get.</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<SongDB>> GetSong(long id)
        {
            var songToReturn = await _repoSong.Get(id);
            if (songToReturn == null)
            {
                return NotFound();
            }
            return Ok(songToReturn);
        }

        [HttpGet("{id}/download")]
        [Produces("application/octet-stream","application/json")]
        public async Task<ActionResult> GetSongFile(long id)
        {
            var song = await _repoSong.Get(id);
            Stream stream = _fileReader.GetFile(song.FilePath);
            return File(stream, "application/octet-stream", "song.mp3");
        }

        /// <summary>
        /// Creates a song, and adds it to the list of songs.
        /// Returns a bad request if the model is invalid, or if the song doesn't exist.
        /// If an artist isn't found, returns a 404.
        /// </summary>
        /// <param name="song">The song we will add to the artist.</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<SongDB>> CreateSong([FromBody] SongDB song)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var artists = await _artistRepo.GetAll();
            var checkIfExists = artists.FirstOrDefault(a => a.Id == song.IdArtist);

            try
            {
                if (checkIfExists == null)
                    throw new InexistingIDException("The song doesn't have a valid Artist ID!");
            }
            catch (InexistingIDException e)
            {
                Debug.WriteLine(e);
                return BadRequest();
            }

            song.Id = await _repoSong.GetNextId();
            await _repoSong.Create(song);

            return Ok(song);
        }

        /// <summary>
        /// Updates a song when all the info is put in.
        /// </summary>
        /// <param name="id">The id of the song we want to update.</param>
        /// <param name="song">The info we will update our song with.</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<ActionResult<SongDB>> UpdateSong(long id, [FromBody] SongDB song)
        {
            if (song == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var artistFromDB = await _repoSong.Get(id);
            if (artistFromDB == null)
            {
                return NotFound();
            }

            try
            {
                if (song.Duration== null || song.Genre == null || song.IdArtist == null || song.Name == null || song.ReleaseDate == null)
                    throw new IncompleteInformationException("Incomplete information!");
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return BadRequest();
            }

            song.Id = artistFromDB.Id;
            song.InternalId = artistFromDB.InternalId;

            await _repoSong.Update(song);
            return new OkObjectResult(song);
        }

        /// <summary>
        /// Partially updates a song. Only updates what an user inputs.
        /// </summary>
        /// <param name="id">The id of the song we want to update</param>
        /// <param name="patchDoc">The info we will update the song with.</param>
        /// <returns></returns>
        /*[HttpPatch("{id}")]
        public IActionResult PartiallyUpdateSong(int id, [FromBody] JsonPatchDocument<SongWID> patchDoc)
        {
            if (patchDoc == null)
            {
                return NotFound();
            }

            SongDto songFromStore = SongsDataStore.Current.Songs.FirstOrDefault(s => s.Id == id);
            SongWID songToPatch = _repo.PartiallyUpdate(id, patchDoc);

            patchDoc.ApplyTo(songToPatch, ModelState);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            TryValidateModel(songToPatch);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            return NoContent();
        }*/

        /// <summary>
        /// Deletes a song.
        /// </summary>
        /// <param name="id">The id of the song we want to delete.</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteSong(long id)
        {
            var song = await _repoSong.Get(id);

            if (song == null)
            {
                return NotFound();
            }

            return Ok(await _repoSong.Delete(id));
        }
    }
}