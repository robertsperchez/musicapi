﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicPlayer.Exceptions
{
    public class RepeatInformationException : Exception
    {
        public RepeatInformationException()
        {
        }

        public RepeatInformationException(string message)
            : base(message)
        {
        }

        public RepeatInformationException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
