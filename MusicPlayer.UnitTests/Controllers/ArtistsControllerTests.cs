﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using MusicPlayer.Controllers;
using MusicPlayer.Models;
using MusicPlayer.Services;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace MusicPlayer.UnitTests.Controllers
{
    [TestFixture]
    public class ArtistsControllerTests
    {
        private Mock<IArtistRepository> _mockRepository;
        private ArtistsController _artistController;

        [SetUp]
        public void Setup()
        {
            _mockRepository = new Mock<IArtistRepository>();
            _artistController = new ArtistsController(_mockRepository.Object);
        }

        [TearDown]
        public void Cleanup()
        {
            _artistController = null;
        }

        [Test]
        public void GetAllTest()
        {
            // Arrange
            IEnumerable<ArtistDB> artists = new List<ArtistDB>();

            _mockRepository.Setup(m => m.GetAll()).ReturnsAsync(artists);
            _artistController = new ArtistsController(_mockRepository.Object);

            // Act 
            ActionResult<IEnumerable<ArtistDB>> a = _artistController.GetAll().Result;

            // Assert
            Assert.IsNotNull(a);
            Assert.IsInstanceOf<ObjectResult>(a.Result);

            ObjectResult result = a.Result as ObjectResult;
            Assert.IsNotNull(result);

            //Assert.AreEqual((int)HttpStatusCode.OK, result.StatusCode);

            Assert.IsInstanceOf<List<ArtistDB>>(result.Value);

            List<ArtistDB> aa = result.Value as List<ArtistDB>;

            Assert.AreEqual(artists.ToList().Count, aa.Count);
            CollectionAssert.AreEqual(artists, aa);
        }

        [Test]
        public async Task GetArtistTest()
        {
            ArtistDB artist = new ArtistDB();
            _mockRepository.Setup(mock => mock.Get(It.IsAny<long>())).ReturnsAsync(artist);
            _artistController = new ArtistsController(_mockRepository.Object);

            ActionResult<ArtistDB> a = await _artistController.GetArtist(1);

            Assert.IsNotNull(a);
            Assert.IsInstanceOf<ActionResult>(a.Result);

            ObjectResult result = a.Result as ObjectResult;
            Assert.IsNotNull(result);

            Assert.AreEqual((int)HttpStatusCode.OK, result.StatusCode);
        }

        [Test]
        public async Task GetArtistNullTest()
        {
            ArtistDB artist = new ArtistDB();
            _artistController = new ArtistsController(_mockRepository.Object);

            ActionResult<ArtistDB> a = await _artistController.GetArtist(1);

            Assert.IsNotNull(a);

            NotFoundResult result = a.Result as NotFoundResult;

            Assert.AreEqual((int)HttpStatusCode.NotFound, result.StatusCode);
        }

        [Test]
        public async Task CreateArtistTest()
        {
            ArtistDB artist = new ArtistDB();
            _artistController = new ArtistsController(_mockRepository.Object);

            ActionResult<ArtistDB> a = await _artistController.CreateArtist(artist);

            Assert.IsNotNull(a);

            ObjectResult result = a.Result as ObjectResult;
            Assert.AreEqual((int)HttpStatusCode.OK, result.StatusCode);
        }

        [Test]
        public async Task UpdateArtistTest()
        {
            ArtistDB artist = new ArtistDB();
            _mockRepository.Setup(mock => mock.Get(It.IsAny<long>())).ReturnsAsync(artist);
            _mockRepository.Setup(mock => mock.Update(artist)).ReturnsAsync(true);
            _artistController = new ArtistsController(_mockRepository.Object);

            artist.Id = 1;
            artist.Country = "Test";
            artist.Genres = new List<string>() { "Test" };
            artist.Members = new List<string>() { "Test" };
            artist.Name = "Test";

            ActionResult<ArtistDB> a = await _artistController.UpdateArtist(artist.Id, artist);

            ObjectResult result = a.Result as ObjectResult;
            Assert.AreEqual((int)HttpStatusCode.OK, result.StatusCode);
        }

        [Test]
        public async Task UpdateArtistBadRequestTest()
        {
            ArtistDB artist = new ArtistDB();
            _mockRepository.Setup(mock => mock.Get(It.IsAny<long>())).ReturnsAsync(artist);
            _mockRepository.Setup(mock => mock.Update(artist)).ReturnsAsync(true);
            _artistController = new ArtistsController(_mockRepository.Object);

            ActionResult<ArtistDB> a = await _artistController.UpdateArtist(artist.Id, artist);
            BadRequestResult result = a.Result as BadRequestResult;

            Assert.AreEqual((int)HttpStatusCode.BadRequest, result.StatusCode);
        }

        [Test]
        public async Task UpdateArtistExceptionTest()
        {
            ArtistDB artist = new ArtistDB();
            ArtistDB nullArtist = new ArtistDB();
            _mockRepository.Setup(mock => mock.Get(It.IsAny<long>())).ReturnsAsync(artist);
            _artistController = new ArtistsController(_mockRepository.Object);

            ActionResult<ArtistDB> a = await _artistController.UpdateArtist(artist.Id, nullArtist);
            BadRequestResult result = a.Result as BadRequestResult;

            Assert.AreEqual((int)HttpStatusCode.BadRequest, result.StatusCode);
        }

        [Test]
        public async Task UpdateArtistNullTest()
        {
            ArtistDB artist = new ArtistDB();
            _artistController = new ArtistsController(_mockRepository.Object);

            ActionResult<ArtistDB> a = await _artistController.UpdateArtist(artist.Id, artist);
            NotFoundResult result = a.Result as NotFoundResult;

            Assert.AreEqual((int)HttpStatusCode.NotFound, result.StatusCode);
        }

        [Test]
        public async Task DeleteArtistTest()
        {
            ArtistDB artist = new ArtistDB();
            _mockRepository.Setup(mock => mock.Get(It.IsAny<long>())).ReturnsAsync(artist);
            _mockRepository.Setup(mock => mock.Delete(It.IsAny<long>())).ReturnsAsync(true);
            _artistController = new ArtistsController(_mockRepository.Object);

            ActionResult a = await _artistController.DeleteArtist(artist.Id);

            ObjectResult result = a as ObjectResult;

            Assert.AreEqual((int)HttpStatusCode.OK, result.StatusCode);
            Assert.IsTrue((bool)result.Value);
        }

        [Test]
        public async Task DeleteArtistNotFoundTest()
        {
            _artistController = new ArtistsController(_mockRepository.Object);

            ActionResult a = await _artistController.DeleteArtist(1);
            NotFoundResult result = a as NotFoundResult;

            Assert.AreEqual((int)HttpStatusCode.NotFound, result.StatusCode);
        }
    }
}
