﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MusicPlayer.Services
{
    public interface IFileReader
    {
        Stream GetFile(string filePath);
    }
}
