﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MusicPlayer.Exceptions;
using MusicPlayer.Models;

namespace MusicPlayer.Services
{
    public class ArtistRepository : IArtistRepository
    {
        private readonly IMusicAPIContext _context;
        public ArtistRepository(IMusicAPIContext context)
        {
            _context = context;
        }

        public async Task Create(ArtistDB artistCreate)
        {
            if (artistCreate == null) //aici nu merge. Se adauga id automat, si atunci nu mai e null
                throw new ArgumentNullException(nameof(artistCreate), "Artist must not be null");

            var artists = await GetAll();
            var checkIfSame = artists.FirstOrDefault(a => a.Name == artistCreate.Name);
            if (checkIfSame != null)
                throw new RepeatInformationException("This artist already exists in the database!");

            await _context.ArtistsCollection.InsertOneAsync(artistCreate);
        }

        public async Task<bool> Delete(long id)
        {
            FilterDefinition<ArtistDB> filter = Builders<ArtistDB>.Filter.Eq(a => a.Id, id);
            DeleteResult deleteResult = await _context.ArtistsCollection.DeleteOneAsync(filter);
            return deleteResult.IsAcknowledged && deleteResult.DeletedCount > 0;
        }

        public async Task<IEnumerable<ArtistDB>> GetAll()
        {
            return await _context.ArtistsCollection.Find(_ => true).ToListAsync();
        }

        public async Task<ArtistDB> Get(long id)
        {
            FilterDefinition<ArtistDB> filter = Builders<ArtistDB>.Filter.Eq(a => a.Id, id);
            return await _context.ArtistsCollection.Find(filter).FirstOrDefaultAsync();
        }

        public async Task<long> GetNextId()
        {
            return await _context.ArtistsCollection.CountDocumentsAsync(new BsonDocument()) + 1;
        }

        /* public async Task<bool> PartiallyUpdate(long id, JsonPatchDocument<ArtistDB> patchDoc)
         {
             throw new NotImplementedException();
         }*/

        public async Task<bool> Update(ArtistDB artist)
        {

            ReplaceOneResult updateResult = await _context.ArtistsCollection.ReplaceOneAsync(filter: a => a.Id == artist.Id, replacement: artist);
            return updateResult.IsAcknowledged && updateResult.ModifiedCount > 0;
        }
    }
}
