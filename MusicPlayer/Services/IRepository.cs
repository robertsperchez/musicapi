﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace MusicPlayer.Services
{
    public interface IRepository<T_obj> where T_obj : class
    {
        //[GET]
        Task<IEnumerable<T_obj>> GetAll();

        //[GET]/id
        Task<T_obj> Get(long id);

        //[POST]
        Task Create(T_obj objCreate);

        //[PUT]/id
        Task<bool> Update(T_obj obj);

        //[DELETE]/id
        Task<bool> Delete(long id);

        Task<long> GetNextId();
    }
}
