﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using MusicPlayer.Exceptions;
using MusicPlayer.Models;
using MusicPlayer.Services;

namespace MusicPlayer.Controllers
{
    [Produces("application/json")]
    [Route("artists")]
    public class ArtistsController : Controller
    {
        private IArtistRepository _repo;
        public ArtistsController(IArtistRepository repo)
        {
            _repo = repo;
        }

        /// <summary>
        /// Returns all the artists.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ArtistDB>>> GetAll()
        {
            return new ObjectResult(await _repo.GetAll());
        }

        /// <summary>
        /// Returns just one artist. Sends a 404 if not found.
        /// </summary>
        /// <param name="id">The id of the artist we want to get.</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<ArtistDB>> GetArtist(long id)
        {
            var artistToReturn = await _repo.Get(id);
            if (artistToReturn == null)
            {
                return NotFound();
            }

            return Ok(artistToReturn);
        }

        /// <summary>
        /// Creates an artist, and adds it to the list of artists. Returns a bad request if the model is invalid.
        /// </summary>
        /// <param name="artistCreate">The artist we want to add.</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<ArtistDB>> CreateArtist([FromBody] ArtistDB artistCreate)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //var artists = await _repo.GetAll();
            //var check = artists.FirstOrDefault(a => a.Name == artistCreate.Name);
            //try
            //{
            //    if(check != null)
            //        throw new RepeatInformationException("This artist already exists in the database!");
            //}
            //catch (RepeatInformationException e)
            //{
            //    Debug.WriteLine(e);
            //    return BadRequest("This artist already exists in the database!");
            //}

            artistCreate.Id = await _repo.GetNextId();
            try
            {
                await _repo.Create(artistCreate);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return Ok(artistCreate);
        }

        /// <summary>
        /// Updates an artist. Only updates it if all the info required is put in.
        /// If the artist doesn't exist, it returns a 404.
        /// If the model is invalid, or there is no info put in by the user, it returns a bad request.
        /// </summary>
        /// <param name="id">The id of the artist we want to update.</param>
        /// <param name="artist">The new info we want to update the artist with.</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<ActionResult<ArtistDB>> UpdateArtist(long id, [FromBody] ArtistDB artist)
        {
            if (artist == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var artistFromDB = await _repo.Get(id);
            if (artistFromDB == null)
            {
                return NotFound();
            }

            try
            {
                if(artist.Country == null || artist.Genres == null || artist.Members == null || artist.Name == null)
                    throw new IncompleteInformationException("Incomplete information!");
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return BadRequest();
            }

            artist.Id = artistFromDB.Id;
            artist.InternalId = artistFromDB.InternalId;

            await _repo.Update(artist);
            return Ok(artist);
        }

        /// <summary>
        /// Partially updates an artist. Only updates what the user inputs.
        /// </summary>
        /// <param name="id">The id of the artist we want to update.</param>
        /// <param name="patchDoc">The info we want to update the artist with.</param>
        /// <returns></returns>
        /* [HttpPatch("{id}")]
        public IActionResult PartiallyUpdateArtist(int id, [FromBody] JsonPatchDocument<ArtistWID> patchDoc)
        {
            if (patchDoc == null)
            {
                return NotFound();
            }

            var artists = ArtistsDataStore.Current.Artists.FirstOrDefault(a => a.Id == id);
            if (artists == null)
            {
                return NotFound();
            }

            var artistToPatch = new ArtistWID();
            _mapper.Map(artists, artistToPatch);

            patchDoc.ApplyTo(artistToPatch, ModelState);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            TryValidateModel(artistToPatch);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _mapper.Map(artistToPatch, artists);
           
            return NoContent();
        } */

        /// <summary>
        /// Deletes an artist. If it isn't found, it return a 404.
        /// </summary>
        /// <param name="id">The id of the artist we want to delete.</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteArtist(long id)
        {
            var artist = await _repo.Get(id);

            if (artist == null)
            {
                return NotFound();
            }

            return Ok(await _repo.Delete(id));
        }
    }
}
