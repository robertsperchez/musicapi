﻿using MusicPlayer.Config;

namespace MusicPlayer.Models
{
    using MongoDB.Driver;

    public class MusicAPIContext : IMusicAPIContext
    {
        public readonly IMongoDatabase _db;

        public MusicAPIContext(MongoDBConfig config)
        {
            var client = new MongoClient(config.ConnectionString);
            _db = client.GetDatabase(config.Database);
        }

        public IMongoCollection<ArtistDB> ArtistsCollection => _db.GetCollection<ArtistDB>("ArtistsCollection");

        public IMongoCollection<SongDB> SongsCollection => _db.GetCollection<SongDB>("SongsCollection");
    }
}
