﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MusicPlayer.Models
{
    public class SongDB
    {
        [BsonId]
        public ObjectId InternalId { get; set; }
        public long Id { get; set; }
        public int IdArtist { get; set; }
        public string Name { get; set; }
        public string Duration { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string Genre { get; set; }
        public string FilePath { get; set; }
    }
}
