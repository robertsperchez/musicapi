﻿namespace MusicPlayer.Models
{
    using MongoDB.Driver;
    public interface IMusicAPIContext
    {
        IMongoCollection<ArtistDB> ArtistsCollection { get; }
        IMongoCollection<SongDB> SongsCollection { get; }
    }
}
